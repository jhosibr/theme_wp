<?php
function custom_theme_customize_register($wp_customize) {
    // Habilitar personalización de tema
    $wp_customize->add_section('header_footer_colors', array(
        'title' => 'Colores del Encabezado y del Pie de Página',
    ));

    // Control para cambiar el color del encabezado
    $wp_customize->add_setting('header_color', array(
        'default' => '#4C4F54',
        'transport' => 'refresh',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_color', array(
        'section' => 'header_footer_colors',
        'label' => 'Color del Encabezado',
    )));

    // Control para cambiar el color del pie de página
    $wp_customize->add_setting('footer_color', array(
        'default' => '#4C4F54',
        'transport' => 'refresh',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_color', array(
        'section' => 'header_footer_colors',
        'label' => 'Color del Pie de Página',
    )));

    // Control para personalizar el logo
    $wp_customize->add_setting('custom_logo');
    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'custom_logo', array(
        'label' => 'Logo',
        'section' => 'title_tagline',
        'settings' => 'custom_logo',
    )));

    // Control para agregar el número de WhatsApp
    $wp_customize->add_setting('whatsapp_number', array(
        'default' => '',
        'transport' => 'refresh',
    ));
    $wp_customize->add_control('whatsapp_number', array(
        'label' => 'Número de WhatsApp',
        'section' => 'header_footer_colors',
        'type' => 'text',
    ));

    // Sección de personalización para el blog o las entradas
    $wp_customize->add_section('blog_customization', array(
        'title' => __('Personalización del Blog o las Entradas'),
        'priority' => 30, 
    ));

     // Opción para el color de fondo del área de contenido del blog o las entradas
     $wp_customize->add_setting('blog_content_background_color', array(
         'default' => '#ffffff',
         'transport' => __('refresh'),
     ));
     $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, __('blog_content_background_color'), array(
         'label' => __('Color de fondo del área de contenido'),
         'section' => __('blog_customization'),
      )));

      // Opción para el color de texto del blog o las entradas
      $wp_customize->add_setting('blog_text_color', array(
          'default' => '#333333',
          'transport' => __('refresh'),
      ));
      $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, __('blog_text_color'), array(
          'label' =>  __('Color de texto'),
          'section' =>  __('blog_customization'),
       )));

       // Opción para el número de entradas a mostrar
       $wp_customize->add_setting('posts_per_page', array(
           'default' => get_option('posts_per_page'),
           'transport' => __('refresh'),
       ));
       $wp_customize->add_control('posts_per_page', array(
           'label' => __('Número de entradas a mostrar'),
           'section' => __('blog_customization'),
           'type' => __('number'),
       ));

       // Opción para el formato de las entradas
       $wp_customize->add_setting('post_format', array(
           'default' => __('list'),
           'transport' => __('refresh'),
       ));
       $wp_customize->add_control('post_format', array(
           'label' => __('Formato de las entradas'),
           'section' => __('blog_customization'),
           //'type' => __('select'),
           //'choices' => array(__('list')=>__('Lista'),__('grid')=>__('Cuadrícula')),
       ));

       // Opción para mostrar o no el extracto
       $wp_customize->add_setting('show_excerpt', array(
           //'default'=>true,
           //'transport'=>'refresh'
       ));
       /*$wp_customize->add_control('show_excerpt',array(
           //'label'=>'Mostrar el extracto',
           //'section'=>'blog_customization',
           //'type'=>'checkbox'
       ));*/
}
add_action('customize_register',  "custom_theme_customize_register");

// Cargar hoja de estilo CSS
function load_custom_styles() {
    wp_enqueue_style('custom-style', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', "load_custom_styles");
