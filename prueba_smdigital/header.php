<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="header" class="site-header">
    <div class="container">
        <div class="header-content">
            <div class="site-logo">
                <?php the_custom_logo(); ?>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <!-- Agrega el menú aquí -->
            </nav>
            <div class="search-box">
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</header>
<div id="content" class="site-content">
