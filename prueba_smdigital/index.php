<?php get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php
        if (have_posts()) :
            while (have_posts()) :
                the_post();
                // Contenido de la entrada
                the_title();
                the_content();
            endwhile;
        else :
            // Contenido por defecto si no hay entradas
            echo 'No se encontraron entradas.';
        endif;
        ?>
    </main>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
