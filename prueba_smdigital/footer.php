</div>
<footer id="footer" class="site-footer">
    <div class="container">
        <!-- Contenido del pie de página -->
    </div>
</footer>

<!-- Botón flotante de WhatsApp -->
<?php
$whatsapp_number = get_theme_mod('whatsapp_number');

if (!empty($whatsapp_number)) {
    echo '<a href="https://wa.me/' . esc_html($whatsapp_number) . '" class="whatsapp-float-button" target="_blank">
              <img src="https://close.marketing/wp-content/uploads/2019/04/whatsapp-icon-logo.png.webp" width="32" height="32">
          </a>';
}
?>
<?php wp_footer(); ?>
</body>
</html>

